package tugas;


import java.io.*;
import java.util.*;
import java.lang.Character;
import java.util.ArrayList;

public class Postfix {
	static char[] inputArray;
	public static Scanner input;
	
	static ArrayList<Character> output = new ArrayList();;
	public static int tinggiPostfix;
	public static int counter;
//	public static Stack inputStack = new Stack();
	
	public static void printArray(Object[][] theArray) {
	    for(int i = 0; i < theArray.length; i++) {
	        for(int j = 0; j < theArray.length; j++) {
	        	if(theArray[i][j].toString() != ";"){
	        		System.out.print(theArray[i][j].toString());//.replaceAll("[\\[\\]]", "") + " "
	        	}
	        	else{
	        		break;
	        	}
	            
	        }
	        System.out.println();
	    }
	}

	// *** Flipping Array ***
	public static void flipInPlace(Object[][] theArray) {
	    for(int i = 0; i < (theArray.length / 2); i++) {
	        Object[] temp = theArray[i];
	        theArray[i] = theArray[theArray.length - i - 1];
	        theArray[theArray.length - i - 1] = temp;
	    }
	}
	
	public static void prosesHasilFinal(char[][] tempArray)
	{
//		char[][] hasilFinal = new char[tempArray.length][tempArray.length];
//		List<List<Character>> hasilFinal = new ArrayList<List<Character>>(tempArray.length);
//		ArrayList<Character>[][] hasilFinal = new ArrayList[tempArray.length][tempArray.length];
		char [][] hasilFinal = new char [tempArray.length][tempArray.length];
		ArrayList<Character>[][] tempFinal = new ArrayList[tempArray.length][tempArray.length];
		
		
		
		
		for(int i = 0; i < tempArray.length;i++)
		{
			for(int j = 0; j < tempArray.length; j++)
			{
				tempFinal[i][j] = new ArrayList<>();
//				hasilFinal[i][j] = new ArrayList<>();
			}
			
		}
		for(int i = 0; i < tempArray.length;i++)
		{
			for(int j = 0; j < tempArray.length; j++)
			{
				if(tempArray[i][j] == '\u0000')
				{
					tempFinal[i][j].add(' ');
				}
				else
				{
					tempFinal[i][j].add(tempArray[i][j]);	
				}	
			}	
		}
		
		
		
		flipInPlace(tempFinal);
		printArray(tempFinal);

	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		int sisaArray = 0;
		int pembatas = 1;
		System.out.print("Masukkan Notasi Postfix yang diinginkan (akhiri notasi dengan tanda \" ; \" ");
		input = new Scanner(System.in);
		String hasilInput = input.nextLine();
		int counterOutput = 0;
		inputArray = hasilInput.toCharArray();
		for(int i = 0; i < inputArray.length; i++)
		{
			if(inputArray[i] == '(' || inputArray[i] == ')' || inputArray[i] == ';') 
				{
				counterOutput++;
				}
			else{
				continue;
			}
			
		}

		char[][] tempFinal = new char[inputArray.length][inputArray.length];
		
		for(int i = 0; i < inputArray.length;i++){
			for(int j = 0; j < pembatas; j++){
				tempFinal[j][i] = prosesPerbaris(inputArray,pembatas)[j];
			}
			pembatas++;
		}		
		System.out.println("Notasi yang diamati adalah = ");
		for(int i = 0; i < inputArray.length-1;i++){
			System.out.print(inputArray[i] + " ");
		}
		System.out.println();
		
		prosesHasilFinal(tempFinal);
		
		System.out.println ("Outputnya adalah =");
		for(int i=0; i < inputArray.length-counterOutput;i++){
			System.out.print(output.get(i).toString().replaceAll("[\\,]", "") + ' ');
		}
		
		
	}
		public static char[] prosesPerbaris(char[] notasi, int jumlahBaca){
		char[] hasilPerbaris = new char[jumlahBaca];
		Stack inputStack;
		inputStack = new Stack();
		if(!inputStack.isEmpty()) {inputStack.removeAllElements();}
		else{inputStack.removeAllElements();}
		
		for(int i = 0; i < jumlahBaca; i++)
		{
			if(notasi[i] == ')')
			{
				while(!inputStack.isEmpty() && (pickOperator(Character.valueOf((Character) inputStack.peek())) && Character.valueOf((Character) inputStack.peek()) != '(' ))
				{
					prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);					
				}
				inputStack.pop();
				
			}
			else if(inputStack.isEmpty() && !Character.isLetterOrDigit(notasi[i]))
			{
				inputStack.push(notasi[i]);		
			}
			else if(notasi[i] =='(' && !Character.isLetterOrDigit(notasi[i]))
			{
				inputStack.push(notasi[i]);
			}
		
			else if(Character.isLetterOrDigit(notasi[i]))
			{
				prosesOutput(notasi[i], jumlahBaca);
			}
			else if(pickOperator(notasi[i]))
			{
				char teratas = Character.valueOf((Character) inputStack.peek());
				if(teratas == '(' && !Character.isLetterOrDigit(notasi[i]))
				{
					inputStack.push(notasi[i]);

				}

				else
				{
					if(Character.isLetterOrDigit(teratas))
					{
//						inputStack.push(' ');
						prosesOutput(notasi[i], jumlahBaca);
					}
//					else if(teratas == ' '){
//						inputStack.pop();
//						inputStack.push(notasi[i]);
//					}
					else if(cekOP(teratas) < cekOP(notasi[i]))
					{
						inputStack.push(notasi[i]);
					}
					else if(cekOP(teratas) > cekOP(notasi[i]) || cekOP(teratas) == cekOP(notasi[i]))
					{
						prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);
						inputStack.push(notasi[i]);
						
					}
					else{
						break;
					}
				}
			}
			
			else
			{
				while(!inputStack.isEmpty())
				{
					prosesOutput(Character.valueOf((Character) inputStack.pop()), jumlahBaca);
				}
				break;
			}
		
	}
		
		for(int i = 0; i< inputStack.size(); i++){
			if(Character.valueOf((Character) inputStack.peek()) == ';'){
				hasilPerbaris[i] = ' ';
			}
			else if(!inputStack.isEmpty()){
				hasilPerbaris[i] = Character.valueOf((Character) inputStack.toArray()[i]);
			}
			else{
				break;
			}
		}

		return hasilPerbaris;
	}
	
	
	public static void prosesOutput(char hasil, int iterasi)
	{
		if(hasil!='(' && iterasi >= inputArray.length && output.size() < inputArray.length)
		{
				output.add(hasil);
		}
		else{
			
		}
	}
	
	public static boolean pickOperator(char op){
		if(op == '+' || op == '-' || op == '/' || op == '^' || op == '*') {
			return true;
		}
		else{
			return false;
		}
	}
	
	static int cekOP(char op){
		switch(op){
			case '+':
			case '-':
				return 0;
			case '*':
			case '/':
				return 1;
			case '^':
				return 2;
			default:
				throw new IllegalArgumentException("Operator "+ op +" tidak ada");
			
		}
	}
}
